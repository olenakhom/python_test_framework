from selenium.webdriver.common.by import By
from framework.dictionary import DICTIONARY as test_data
import os

BASE_URL = os.environ.get('BASE_URL')


class MainPageLocator:
    SUCCESS_POPUP = (By.XPATH, '//*[@id="toast-container"][1]')


class LogoLocator:
    LOGO = (By.XPATH, "//img[contains(@src, 'logo.png')]")
