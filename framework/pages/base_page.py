#!/usr/bin/env python
# -*- coding: utf-8 -*-

from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver import ActionChains
from framework.locators import LogoLocator
from selenium.webdriver.support import expected_conditions as EC
from framework.locators import BASE_URL
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.keys import Keys


class BasePage:

    def __init__(self, driver, base_url=BASE_URL):
        self.driver = driver
        self.base_url = base_url

    def open(self, url=""):
        url = self.base_url + url
        self.driver.get(url)
        self.wait_for_angular()

    def is_logo_present(self):
        return self.is_element_present(*LogoLocator.LOGO)

    def find_element(self, *locator):
        return self.driver.find_element(*locator)

    def wait_for_angular(self):
        self.driver.set_script_timeout(30)
        self.driver.execute_async_script("""
            callback = arguments[arguments.length - 1];
            angular.element('html').injector().get('$browser').notifyWhenNoOutstandingRequests(callback);""")

    def click(self, *locator):
        self.wait_until_element_to_be_clickable(locator)
        result = self.driver.find_element(*locator).click()
        self.wait_for_angular()
        return result

    def type(self, text, *locator):
        element = self.driver.find_element(*locator)
        element.clear()
        element.send_keys(text)
        element.send_keys("\t")  # TAB should move a focus out of text input
        self.wait_for_angular()

    def clear(self, *locator):
        element = self.driver.find_element(*locator)
        element.clear()

    def get_title(self):
        return self.driver.title

    def get_current_url(self):
        return self.driver.current_url

    def hover(self, *locator):
        element = self.driver.find_element(*locator)
        hover = ActionChains(self.driver).move_to_element(element)
        hover.perform()

    def is_element_present(self, *locator):
        try:
            self.find_element(*locator)
        except NoSuchElementException:
            return False
        return True

    def wait_until_element_is_visible(self, locator, timeout=10):
        try:
            _d = self.driver
            WebDriverWait(_d, timeout).until(EC.visibility_of_element_located(locator))
        except TimeoutException:
            raise AssertionError('It takes more than {} sec to load an element'.format(timeout))

    def is_element_invisible(self, *locator):
        try:
            self.wait_until_invisibility_of_element_located(locator)
            self.find_element(*locator)
        except NoSuchElementException:
            return False
        return True

    def is_element_visible(self, *locator):
        try:
            self.wait_until_visibility_of_element_located(locator)
            self.find_element(*locator)
        except NoSuchElementException:
            return False
        return True

    def wait_until_element_to_be_clickable(self, locator,  timeout=30):
        try:
            WebDriverWait(self.driver, timeout).until(EC.element_to_be_clickable(locator))
        except TimeoutException:
            raise AssertionError('It takes more than {} sec to load an element'.format(timeout))

    def wait_until_visibility_of_element_located(self, locator,  timeout=10):
        try:
            WebDriverWait(self.driver, timeout).until(EC.visibility_of_element_located(locator))
        except TimeoutException:
            raise AssertionError('It takes more than {} sec to load an element'.format(timeout))

    def wait_until_invisibility_of_element_located(self, locator,  timeout=10):
        try:
            WebDriverWait(self.driver, timeout).until(EC.invisibility_of_element_located(locator))
        except TimeoutException:
            raise AssertionError('It takes more than {} sec to load an element'.format(timeout))

    def wait_until_text_to_be_present_in_element(self, *locator, text='.', timeout=5):
        try:
            WebDriverWait(self.driver, timeout).until(EC.text_to_be_present_in_element_value(locator, text))
        except TimeoutException:
            raise AssertionError('It takes more than {} sec to load an element'.format(timeout))

    def get_value_from_element(self, *locator):
        self.wait_until_text_to_be_present_in_element(*locator)
        return self.find_element(*locator).get_attribute("value")

    def get_text(self, *locator):
        if self.is_element_visible(*locator):
            element = self.find_element(*locator)
            return element.text
        else:
            return None

    def is_popup_present(self, *locator, timeout=5):
        _d = self.driver
        try:
            WebDriverWait(_d, timeout).until(lambda _d: _d.find_element(*locator))
        except Exception:
            return False
        return True

    def press_enter(self, *locator):
        self.driver.find_element(*locator).send_keys(Keys.ENTER)

    def get_number_of_elements(self, *locator):
        self.driver.implicitly_wait(5)
        number_of_elements = len(self.driver.find_elements(*locator))
        self.driver.implicitly_wait(20)
        return number_of_elements
