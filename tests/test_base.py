import unittest
from framework.driver_wrapper import Driver
from framework.pages.main_page import MainPage
from framework.locators import BASE_URL


class TestBase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        base_url = BASE_URL
        cls.driver = Driver.get_driver()
        cls.home_page = MainPage(cls.driver, base_url)
        cls.home_page.open()

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

    def tearDown(self):
        filename = "public/{}_teardown.png".format(self.id())
        if self.driver is not None:
            Driver.screenshot(self.driver, filename)
