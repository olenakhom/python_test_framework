#!/bin/bash

# Path to base folder of tests
export PYTHONPATH="${PYTHONPATH}:/home/olenakhom/PycharmProjects/google"

# Type of driver connection "LOCAL" or "REMOTE"
export SELENIUM_CONNECTION="LOCAL"

# Local driver location
export SELENIUM_DRIVER_PATH="/home/olenakhom/PycharmProjects/google/framework/resources/chromedriver"

# Selenium RC URL
export SELENIUM_RC_URL="http://google.com"

# Browser type "chrome", "firefox", "phantomjs"
export SELENIUM_BROWSER="chrome"

# Test System URL "http://localhost" - for LOCAL connection, "http://ecomap" - for REMOTE connection
export BASE_URL="http://google.com"

# Run The tests in project folder
# nosetests -v --nocapture --nologcapture /home/olenakhom/PycharmProjects/google/tests/test_search.py
 nosetests -v --nocapture --nologcapture /home/olenakhom/PycharmProjects/google/tests
